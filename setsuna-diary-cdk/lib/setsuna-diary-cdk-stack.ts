import { RemovalPolicy, Stack, StackProps } from 'aws-cdk-lib';
import { Construct } from 'constructs';
// import * as sqs from 'aws-cdk-lib/aws-sqs';
import * as S3 from "aws-cdk-lib/aws-s3";
import * as ec2 from 'aws-cdk-lib/aws-ec2';

export class SetsunaDiaryCdkStack extends Stack {
  constructor(scope: Construct, id: string, props?: StackProps) {
    super(scope, id, props);

    const videoBucket = new S3.Bucket(this, `${id}-assets`, {
      bucketName: "private-assets",
      removalPolicy: RemovalPolicy.DESTROY,
    });


    // The code that defines your stack goes here

    // example resource
    // const queue = new sqs.Queue(this, 'SetsunaDiaryCdkQueue', {
    //   visibilityTimeout: cdk.Duration.seconds(300)
    // });
  }
}
