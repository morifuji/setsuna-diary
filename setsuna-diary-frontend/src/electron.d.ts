export interface IElectronAPI {
    closeWindow: () => void
    onChangeWindowHeight: () => void
    convertUrlToBase64: (url: string) => Promise<string>
  }
  
  declare global {
    interface Window {
      electronAPI: IElectronAPI
    }
  }