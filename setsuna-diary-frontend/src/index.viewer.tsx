import './index.css';
import React from 'react';
import ReactDOM from 'react-dom';
import Viewer from './pages/viewer';
import reportWebVitals from './reportWebVitals';

import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from "@apollo/client";
import { createTheme, ThemeProvider } from '@mui/material';
console.log(process.env.REACT_APP_GRAPHQL_ENDPOINT)
const client = new ApolloClient({
  uri: process.env.REACT_APP_GRAPHQL_ENDPOINT,
  cache: new InMemoryCache(),
  headers: {
    "x-setsuna-id": "7496a6fc-66be-4bc3-ac6b-e1d2375013c2"
  }
});


const theme = createTheme({
  palette: {
    primary: {
      main: "#000",
    },
    secondary: {
      main: '#6a1b9a',
    },
  },
  typography: {
    fontFamily: "'M PLUS Rounded 1c'"
  }
});

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <ApolloProvider client={client}>
        <Viewer />
      </ApolloProvider>
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
