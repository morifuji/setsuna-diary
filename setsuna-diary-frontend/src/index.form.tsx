import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import {Form} from './pages/form/form';
import reportWebVitals from './reportWebVitals';

const App = () => {
  const onSent = ()=>{
    new Notification("記録完了💪")

    setTimeout(()=>{
      window.electronAPI.closeWindow()
    }, 300)
  }

  return <Form onClick={()=>{}} onSent={onSent}/>
}

ReactDOM.render(
  <React.StrictMode>
    <App/>
  </React.StrictMode>,
  document.getElementById('root')
);

const resizeObserver = new ResizeObserver(entries => 
  window.electronAPI.onChangeWindowHeight()
)

// start observing a DOM node
resizeObserver.observe(document.body)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
