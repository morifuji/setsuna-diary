import { Backdrop, Box, Fab, Snackbar, Typography, useTheme } from "@mui/material";
import { PropsWithChildren, useCallback, useState } from "react";
import { Form } from "../form/form";
import { TopicCard } from "./topic-card";
import { Topic } from "./_type";
import { date2YearMonth } from "./_util";

type TopicRefType = { [k: string]: HTMLElement | null; }

export const Content = (props: PropsWithChildren<{ onDataChanged: () => void, topics: Topic[], topicRef: React.MutableRefObject<TopicRefType>, showImageDialog: (imageSrc: string) => void }>) => {

  const theme = useTheme()
  const [isShowForm, setIsShowForm] = useState(false)
  const [isShowSuccessMessage, setIsShowSuccessMessage] = useState(false)

  const onSent = useCallback(() => {
    setIsShowSuccessMessage(true)
    setIsShowForm(false)
    props.onDataChanged()
  }, [props])

  return <Box sx={{ maxWidth: "100%", maxHeight: "100vh", overflow: "scroll", flexGrow: 1, backgroundColor: theme.palette.grey[100], position: "relative" }}>
    {props.topics.reduce((nodeList, topic, i) => {
      const topicCard = <TopicCard key={topic.id} topic={topic} onClick={props.showImageDialog}></TopicCard>
      const previewTopicDate = i === 0 ? null : props.topics[i - 1].page.date

      if (previewTopicDate === null || previewTopicDate !== topic.page.date) {
        const section = <Box key={`section_${topic.id}`} pl={1}>
          <Typography variant="subtitle1" sx={{fontSize: "0.8rem"}} color="secondary">{topic.page.date}</Typography>
        </Box>;
        const yearMonth = date2YearMonth(topic.page.date)
        if (previewTopicDate === null || date2YearMonth(previewTopicDate) !== yearMonth) {
          return nodeList.concat(<div key={topic.id}>
            <Typography style={{ textAlign: "center" }} ref={el => props.topicRef.current[yearMonth] = el}>
              ----------
              <span style={{ fontSize: "1.7rem" }}>{yearMonth.split("-")[0]}</span>年
              <span style={{ fontSize: "1.7rem" }}>{yearMonth.split("-")[1]}</span>月
              ----------
            </Typography>
            {section}
            {topicCard}
          </div>)
        }

        return nodeList.concat(<div key={topic.id}>
          {section}
          {topicCard}
        </div>)
      }

      return nodeList.concat(topicCard)
    }, [] as JSX.Element[])}
    <Fab sx={{ position: "fixed", right: 15, bottom: 15 }} size="large" variant="circular" color="secondary"
      onClick={() => setIsShowForm(true)}>
      <Typography style={{fontSize: "1.5rem"}}>📝</Typography>
    </Fab>
    <Backdrop
      sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
      open={isShowForm}
      onClick={() => setIsShowForm(false)}
    >
      <Form onSent={onSent} onClick={ (e) => {e.stopPropagation()}}/>
    </Backdrop>
    <Snackbar
      open={isShowSuccessMessage}
      autoHideDuration={6000}
      onClose={() => setIsShowSuccessMessage(false)}
      message="記録が完了しました🚀"
    />
  </Box>
}
