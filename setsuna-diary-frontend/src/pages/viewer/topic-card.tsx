import React, {
  PropsWithChildren,
} from "react";
import * as dateFns from "date-fns";
import { Box, Typography, useTheme } from "@mui/material";

import { getTopicsDesc_topics } from "./__generated__/getTopicsDesc";


export const TopicCard = React.forwardRef(
  (props: PropsWithChildren<{ onClick?: (imageSrc: string) => void, topic: getTopicsDesc_topics }>, ref) => {
    const theme = useTheme()
    const content = props.topic.content
    
    return (
      <Box ref={ref} sx={{ display: "flex" }}>
        <Box pl={2} pr={1}>
          <Typography sx={{fontSize: "0.6rem",  color: theme.palette.grey[700]}} lineHeight="24px">{dateFns.format(dateFns.parseISO(props.topic.createdAt), "HH:mm")}</Typography>
        </Box>
        <div>
          <Typography style={{overflow: "hidden",wordBreak: "break-word", whiteSpace: "pre-wrap"}}>
            {content}
          </Typography>
          <Box sx={{ display: "flex", gap: "10px" }}>
            {props.topic.assets.map((asset, i) =>
              <img
                key={i}
                style={{ width: 50, height: 50, objectFit: "cover" }}
                alt="##"
                src={asset.url}
                onClick={() => props.onClick!(asset.url)}
              />
            )}
          </Box>
        </div>
      </Box>
    );
  }
);
