/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getTopicsDesc
// ====================================================

export interface getTopicsDesc_topics_page {
  __typename: "Page";
  date: string;
}

export interface getTopicsDesc_topics_assets {
  __typename: "Asset";
  assetType: string;
  metadata: string | null;
  url: string;
}

export interface getTopicsDesc_topics {
  __typename: "Topic";
  page: getTopicsDesc_topics_page;
  id: string;
  content: string;
  assets: getTopicsDesc_topics_assets[];
  createdAt: any;
}

export interface getTopicsDesc {
  topics: getTopicsDesc_topics[];
}

export interface getTopicsDescVariables {
  maxDate?: any | null;
}
