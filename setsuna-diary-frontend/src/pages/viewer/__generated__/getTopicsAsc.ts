/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getTopicsAsc
// ====================================================

export interface getTopicsAsc_topics_page {
  __typename: "Page";
  date: string;
}

export interface getTopicsAsc_topics_assets {
  __typename: "Asset";
  assetType: string;
  metadata: string | null;
  url: string;
}

export interface getTopicsAsc_topics {
  __typename: "Topic";
  page: getTopicsAsc_topics_page;
  id: string;
  content: string;
  assets: getTopicsAsc_topics_assets[];
  createdAt: any;
}

export interface getTopicsAsc {
  topics: getTopicsAsc_topics[];
}

export interface getTopicsAscVariables {
  minDate?: any | null;
}
