import {
  useCallback,
  useRef,
  useState,
} from "react";
import "./index.css";
import { Backdrop, Box, CircularProgress } from "@mui/material";

import { gql } from 'graphql.macro';
import { useQuery } from "@apollo/client/react";
import { getTopicsDesc, getTopicsDescVariables } from "./__generated__/getTopicsDesc";
import { Sidebar } from "./sidebar";
import { date2YearMonth } from "./_util";
import { Content } from "./content";
import { ImageModal } from "./image-modal";

const queryGetTopicsDesc = gql`
query getTopicsDesc($maxDate: DateTime){
  topics(order: CREATED_AT_DESC, maxDate: $maxDate){
    page{
      date
    }
    id
    content
    assets{
      assetType
      metadata
      url
    }
    createdAt
  }
}
`

const useImageDialog = () => {
  const [imageDialogSrc, setImageDialogSrc] = useState<string | null>(null);
  const [isShowImageDialog, setIsShowImageDialog] = useState(false);

  const hide = useCallback(() => {
    setIsShowImageDialog(false);
  }, [setIsShowImageDialog]);
  const show = useCallback(
    (imageSrc: string) => {
      setIsShowImageDialog(true);
      setImageDialogSrc(imageSrc);
    },
    [setIsShowImageDialog, setImageDialogSrc]
  );
  return [imageDialogSrc, isShowImageDialog, hide, show] as const;
};

const Viewer = () => {
  const { loading, error, data, refetch } = useQuery<getTopicsDesc, getTopicsDescVariables>(queryGetTopicsDesc,
    {
      variables: {
        maxDate: undefined
      },
    }
  );

  const [imageDialogSrc, isShowImageDialog, hideImageDialog, showImageDialog] =
    useImageDialog();
  const onClickYearMonth = useCallback((yearMonth: string) => {
    topicRef?.current[yearMonth]?.scrollIntoView({
      behavior: "smooth",
    })
  }, []);

  const topicRef = useRef<{ [k: string]: HTMLElement | null }>({});

  if (loading) {
    return <Backdrop open>
      <Box sx={{ display: "flex", width: "100vw", height: "100vh", alignItems: "center", justifyContent: "center" }}>
        <CircularProgress />
      </Box>
    </Backdrop>
  }
  if (error) return <p>Error :(</p>;
  const topics = data!.topics

  const yearMonthList = Array.from(new Set(topics.map(topic => date2YearMonth(topic.page.date)))).sort().reverse()

  return (
    <Box sx={{ display: "flex" }}>
      <Sidebar yearMonthList={yearMonthList} onClickYearMonth={onClickYearMonth} />
      <Content showImageDialog={showImageDialog} topicRef={topicRef} topics={topics} onDataChanged={() => { console.log("##"); refetch() }} />
      <ImageModal
        isShow={isShowImageDialog}
        imageSrc={imageDialogSrc}
        onClose={hideImageDialog}
      />
    </Box>
  );
};

export default Viewer;
