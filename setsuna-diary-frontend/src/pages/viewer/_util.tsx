export const date2YearMonth = (date: string) => date.split("-").slice(0, 2).join("-")
