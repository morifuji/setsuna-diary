import { Box, Typography, useTheme } from "@mui/material";
import { PropsWithChildren } from "react";

export const Sidebar = (props: PropsWithChildren<{yearMonthList: string[], onClickYearMonth: (yearMonth: string)=>void}>) => {
  const theme = useTheme()
return <Box
sx={{
  width: 200,
  padding: "10px",
  backgroundColor: theme.palette.primary.main,
  color: theme.palette.primary.contrastText,
  flexShrink: 0,
  maxHeight: "100vh",
  overflow: "scroll",
}}
>
{props.yearMonthList.map(yearMonth => {
  return (
    <Typography key={yearMonth} onClick={() => props.onClickYearMonth(yearMonth)}>
      {yearMonth.split("-").join("年") + "月"}
    </Typography>
  );
})}
</Box>
}