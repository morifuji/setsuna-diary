import { Backdrop } from "@mui/material";
import { MouseEventHandler, PropsWithChildren } from "react";

export const ImageModal = (
  props: PropsWithChildren<{
    isShow: boolean;
    imageSrc: string | null;
    onClose: MouseEventHandler;
  }>
) => {
  return (
    <>
      <Backdrop
        sx={{ color: "#fff", zIndex: (theme) => theme.zIndex.drawer + 1 }}
        open={props.isShow}
        onClick={props.onClose}
      >
        <img
          style={{ width: "90vh", height: "90vh", objectFit: "contain" }}
          src={props.imageSrc!}
          alt="##"
        />
      </Backdrop>
    </>
  );
};
