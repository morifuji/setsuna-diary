import React from 'react';
import { render, screen } from '@testing-library/react';
import {Form} from './form';

test('renders learn react link', () => {
  render(<Form onSent={()=>{}}  />);
  const linkElement = screen.getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
