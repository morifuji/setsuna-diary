import React, { PropsWithChildren, useCallback, useEffect, useRef, useState } from 'react';
import './form.css';
import { Alert, Backdrop, Box, CircularProgress, Paper, Snackbar, TextField } from '@mui/material';
import axios from 'axios';

const getBase64 = (file: File) => new Promise((resolve, reject) => {
  var reader = new FileReader();
  reader.readAsDataURL(file);
  reader.onload = () => {
    console.log(reader.result);
    resolve(reader.result)
  };
  reader.onerror = (error) => {
    console.log('Error: ', error);
    reject(error)
  };
})

const getImageUrlListFromNode = () => {
  const elementList: NodeListOf<HTMLImageElement> = document.querySelectorAll("img[data-image]")
  const imageUrlList: string[] = []
  elementList.forEach(element => {
    imageUrlList.push(element.src)
  })

  return imageUrlList
}

const GRAPHQL_ENDPOINT = process.env.REACT_APP_GRAPHQL_ENDPOINT || "http://localhost:3000/graphql"

export const Form = (props: PropsWithChildren<{ onSent: () => void, onClick?: React.MouseEventHandler<HTMLDivElement> }>) => {

  const [content, setContent] = useState("")

  const [isLoading, setIsLoading] = useState(false)

  const [message, setMessage] = useState<{ severity: 'success' | 'error', text: string } | null>(null)

  window.addEventListener("paste", () => {
    console.log("paste window!")
  })

  const [imageFileObjectList, setImageFileObjectList] = useState<File[]>([])
  const [imageBase64StringList, setImageBase64StringList] = useState<string[]>([])
  const [imageHtmlList, setImageHtmlList] = useState<string[]>([])


  const handleKeyUp = useCallback(async (e: React.KeyboardEvent<HTMLDivElement>) => {
    if (e.key !== 'Enter' || !e.shiftKey) {
      return
    }
    e.preventDefault();
    setIsLoading(true)

    const base64FromImageFileObjectList = await Promise.all(imageFileObjectList.map(file => getBase64(file))) as string[]
    /** コンテンツ一覧 */
    const contentList = imageBase64StringList.concat(...base64FromImageFileObjectList).concat(...getImageUrlListFromNode())
    console.log(contentList)

    await axios.post(GRAPHQL_ENDPOINT, {
      variables: {
        content,

        assets: contentList.map(content => ({
          assetType: "IMAGE",
          content: content
        }))
      },
      query: `
      mutation saveTopic($content: String!, $assets: [AssetInput!]!){
        saveTopic(topic: {content: $content, assets: $assets}){
          id
        }
      }`
    }, {
      headers: {
        "x-setsuna-id": "e-4bc3-ac6b-e1d2375013c2",
        "content-type": "application/json"
      },
    }).then(response => {
      const isError = "errors" in response.data
      if (isError) throw Error(response.data.errors)

      setImageFileObjectList([])
      setImageBase64StringList([])
      setImageHtmlList([])
      setContent("")

      props.onSent()
    }).catch(e => {
      setMessage({ severity: "error", text: "不明なエラーが発生しました" })
    })
      .finally(() => {
        setIsLoading(false)
      })
  }, [content, imageBase64StringList, imageFileObjectList, props])

  const onPaste = (e: React.ClipboardEvent<HTMLDivElement>) => {

    if (e.clipboardData?.items?.length >= 1) {
      const file = e.clipboardData?.items[0].getAsFile()
      if (file !== null) {
        setImageFileObjectList(imageFileObjectList.concat(file))
        e.preventDefault()
      } else {

        e.clipboardData?.items[0].getAsString((async b => {
          if (b.includes("<img")) {
            setImageHtmlList(imageHtmlList.concat(formatHtml(b, "" + imageHtmlList.length)))
            return
          }

          if (b.startsWith("https")) {
            const base64 = await window.electronAPI.convertUrlToBase64(b)
            setImageBase64StringList(imageBase64StringList.concat(base64))
            return
          }

          if (b.startsWith("data:image")) {
            setImageBase64StringList(imageBase64StringList.concat(b))
            return
          }
        }))
      }
    }

  }

  const formatHtml = (rawHtml: string, id: string) => {
    const splitted = rawHtml.split("<img")
    return splitted[0] + `<img data-image style='width: 50px; height: 50px; object-fit: cover;' ` + splitted[1]
  }

  const inputElement = useRef<HTMLInputElement | null>(null);

  useEffect(()=>{
    inputElement.current!.focus()
  }, [])

  return (
    <Paper sx={{ backgroundColor: "rgba(255, 255, 255, 0.9)" }} onClick={props.onClick}>
      <Box sx={{ padding: 3 }}>
        <Backdrop
          sx={{ color: '#fff', zIndex: (theme) => theme.zIndex.drawer + 1 }}
          open={isLoading}
        >
          <CircularProgress color="inherit" />
        </Backdrop>
        <Box sx={{ padding: 1, display: 'flex', alignItems: 'center' }}>
          <span style={{ paddingRight: 15 }}>✏️</span>
          <TextField
            inputRef={inputElement}
            onPasteCapture={(e) => onPaste(e)}
            id="outlined-textarea"
            label="今何考えてる？"
            multiline
            minRows={3}
            maxRows={3}
            fullWidth
            helperText="Shift+Enterで送信📝、画像🌄もペーストできるよ！"
            value={content}
            onKeyDown={e => handleKeyUp(e)}
            onChange={e => { console.log('yaaaa'); setContent(e.target.value) }}
          />
        </Box>
        <Box sx={{ padding: 1, marginLeft: "31px", display: 'flex', flexWrap: "wrap" }}>
          {imageFileObjectList.map(i => <img style={{ padding: "5px", width: 50, height: 50, objectFit: "cover" }} src={URL.createObjectURL(i)} alt="hoge" />)}
          {imageBase64StringList.map(i => <img style={{ padding: "5px", width: 50, height: 50, objectFit: "cover" }} src={i} alt="hoge" />)}
          {imageHtmlList.map(i => <div style={{ padding: "5px", width: 50, height: 50, objectFit: "cover" }} dangerouslySetInnerHTML={{ __html: i }} />)}
        </Box>
        <Snackbar
          open={message !== null}
          onClose={() => setMessage(null)}
          autoHideDuration={5000}
        >
          <Alert severity={message?.severity}>{message?.text}</Alert>
        </Snackbar>
      </Box>
    </Paper>

  );
}
