# setsuna-diary

## demo

#### web app

![](./setsuna-diary-frontend.gif)

#### desktop app

![](./setsuna-diary-desktop.gif)

#### iOS/Android app

<img src="./setsuna-diary-mobile.gif" height="500">

## ec2

```
scp -r -i ~/.ssh/setsuna-backend-keypair.pem ./*  ec2-user@18.183.203.91:/home/ec2-user/app
```

## electron

```
 yarn build:form && cp -R ./build ../setsuna-diary-desktopapp && rm -rf ../setsuna-diary-desktopapp/view && cd ../setsuna-diary-desktopapp && mv build view && ../setsuna-diary-frontend 
 ```

## env

#### save

```
aws ssm put-parameter \
    --name /backend/env \
    --type SecureString \
    --region ap-northeast-1 \
    --value file://./.env \
    --overwrite \
    --profile setsuna-diary
```

#### get

```
aws ssm get-parameter 
    --with-decryption \
    --name /backend/env \
    --output text \
    --query 'Parameter.Value' \
    --profile setsuna-diary \
    --region ap-northeast-1 > .env
```