set -ue
image_tag=$(date '+%Y%m%d_%H%M%S')
docker build -t setsuna-diary-backend .

aws ecr get-login-password --region ap-northeast-1 --profile setsuna-diary | docker login --username AWS --password-stdin 262011991733.dkr.ecr.ap-northeast-1.amazonaws.com
docker tag setsuna-diary-backend:latest 262011991733.dkr.ecr.ap-northeast-1.amazonaws.com/setsuna-diary-backend:$image_tag

docker push 262011991733.dkr.ecr.ap-northeast-1.amazonaws.com/setsuna-diary-backend:$image_tag
