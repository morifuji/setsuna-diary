-- DropForeignKey
ALTER TABLE "Asset" DROP CONSTRAINT "Asset_topicId_fkey";

-- DropForeignKey
ALTER TABLE "Page" DROP CONSTRAINT "Page_authorId_fkey";

-- DropForeignKey
ALTER TABLE "Topic" DROP CONSTRAINT "Topic_date_authorId_fkey";

-- CreateTable
CREATE TABLE "Theme" (
    "id" TEXT NOT NULL,
    "theme" VARCHAR(255) NOT NULL,

    CONSTRAINT "Theme_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "TopicTheme" (
    "topicId" TEXT NOT NULL,
    "themeId" TEXT NOT NULL,

    CONSTRAINT "TopicTheme_pkey" PRIMARY KEY ("topicId","themeId")
);

-- AddForeignKey
ALTER TABLE "Page" ADD CONSTRAINT "Page_authorId_fkey" FOREIGN KEY ("authorId") REFERENCES "Author"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Topic" ADD CONSTRAINT "Topic_date_authorId_fkey" FOREIGN KEY ("date", "authorId") REFERENCES "Page"("date", "authorId") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "Asset" ADD CONSTRAINT "Asset_topicId_fkey" FOREIGN KEY ("topicId") REFERENCES "Topic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TopicTheme" ADD CONSTRAINT "TopicTheme_topicId_fkey" FOREIGN KEY ("topicId") REFERENCES "Topic"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "TopicTheme" ADD CONSTRAINT "TopicTheme_themeId_fkey" FOREIGN KEY ("themeId") REFERENCES "Theme"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
