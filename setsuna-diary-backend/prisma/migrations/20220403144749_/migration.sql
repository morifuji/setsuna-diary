/*
  Warnings:

  - The `assetType` column on the `Asset` table would be dropped and recreated. This will lead to data loss if there is data in the column.

*/
-- CreateEnum
CREATE TYPE "AssetType" AS ENUM ('IMAGE', 'VIDEO', 'TWEET');

-- AlterTable
ALTER TABLE "Asset" DROP COLUMN "assetType",
ADD COLUMN     "assetType" "AssetType" NOT NULL DEFAULT E'IMAGE';
