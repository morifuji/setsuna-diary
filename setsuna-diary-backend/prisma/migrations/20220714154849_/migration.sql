/*
  Warnings:

  - You are about to drop the column `theme` on the `Theme` table. All the data in the column will be lost.
  - Added the required column `word` to the `Theme` table without a default value. This is not possible if the table is not empty.
  - Added the required column `position` to the `TopicTheme` table without a default value. This is not possible if the table is not empty.

*/
-- AlterTable
ALTER TABLE "Theme" DROP COLUMN "theme",
ADD COLUMN     "word" VARCHAR(255) NOT NULL;

-- AlterTable
ALTER TABLE "TopicTheme" ADD COLUMN     "position" INTEGER NOT NULL;
