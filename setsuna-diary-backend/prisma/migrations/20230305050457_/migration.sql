-- CreateEnum
CREATE TYPE "TopicSource" AS ENUM ('TWITTER', 'WEB', 'NATIVE_APP');

-- AlterTable
ALTER TABLE "Topic" ADD COLUMN     "source" "TopicSource" NOT NULL DEFAULT E'WEB';
