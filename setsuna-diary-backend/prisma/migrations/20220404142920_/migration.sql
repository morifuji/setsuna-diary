/*
  Warnings:

  - You are about to drop the column `url` on the `Asset` table. All the data in the column will be lost.
  - Made the column `key` on table `Asset` required. This step will fail if there are existing NULL values in that column.

*/
-- AlterTable
ALTER TABLE "Asset" DROP COLUMN "url",
ALTER COLUMN "key" SET NOT NULL;
