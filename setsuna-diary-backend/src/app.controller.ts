import {
  Controller,
  Get,
  Redirect,
  UseFilters,
  UseGuards,
} from '@nestjs/common';
import { AppService } from './app.service';
import { AuthGuard } from '@nestjs/passport';
import { AuthExceptionFilter } from './auth/auth-exception.filter';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  @UseFilters(new AuthExceptionFilter())
  @UseGuards(AuthGuard('basic'))
  @Redirect('/app', 301)
  getHello(): string {
    return this.appService.getHello();
  }
}
