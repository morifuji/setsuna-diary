import { ApolloDriver, ApolloDriverConfig } from '@nestjs/apollo';
import { ServeStaticModule } from '@nestjs/serve-static';
import { Module } from '@nestjs/common';
import { GraphQLModule } from '@nestjs/graphql';
import { join } from 'path';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { PrismaService } from './prisma/prisma.service';
import { AssetResolver } from './resolver/resolver.asset';
import { AuthorResolver } from './resolver/resolver.author';
import { PageResolver } from './resolver/resolver.page';
import { TopicResolver } from './resolver/resolver.topic';
import { ThemeResolver } from './resolver/resolver.theme';
import { StorageService } from './service/storage';
import { StorageDataLoader } from './service/storage.dataloader';
import { AuthModule } from './auth/auth.module';
// import { AnalysisService } from './service/analysis';
import { TwitterModule } from './twitter/twitter.module';
import { ScheduleModule } from '@nestjs/schedule';

@Module({
  imports: [
    ScheduleModule.forRoot(),
    AuthModule,
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      serveRoot: '/app',
    }),
    GraphQLModule.forRoot<ApolloDriverConfig>({
      driver: ApolloDriver,
      debug: true,
      playground: true,
      autoSchemaFile: join(process.cwd(), 'graphql/schema.gql'),
    }),
    TwitterModule,
  ],
  controllers: [AppController],
  providers: [
    PrismaService,
    StorageService,
    StorageDataLoader,
    TopicResolver,
    ThemeResolver,
    AppService,
    PageResolver,
    AuthorResolver,
    AssetResolver,
    // AnalysisService,
  ],
})
export class AppModule {}
