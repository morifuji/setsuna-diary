import { BasicStrategy as Strategy } from 'passport-http';
import { Injectable, UnauthorizedException } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';

@Injectable()
export class BasicStrategy extends PassportStrategy(Strategy) {
  public validate = async (username, password): Promise<boolean> => {
    if (process.env.BASIC_AUTH_USERNAME !== username || process.env.BASIC_AUTH_PASSWORD !== password) {
      throw new UnauthorizedException();
    }

    return true;
  };
}
