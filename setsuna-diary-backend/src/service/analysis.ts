import { Injectable } from '@nestjs/common';
import kuromoji from 'kuromoji';

type ProperNoun = {
  position: number;
  word: string;
};

const tokenizer: Promise<kuromoji.Tokenizer<kuromoji.IpadicFeatures>> =
  new Promise((resolve, reject) => {
    kuromoji
      .builder({
        dicPath: './kuromoji-dict',
      })
      .build((err, _tokenizer) => {
        if (!!err) {
          reject(err);
        }
        resolve(_tokenizer);
      });
  });

@Injectable()
export class AnalysisService {
  async extractProperNounList(text: string) {
    const t = await tokenizer;
    const pnList: ProperNoun[] = [];
    let consecutiveWord: ProperNoun | null = null;
    t.tokenize(text).forEach((result, i) => {
      if (result.pos === '名詞') {
        if (consecutiveWord === null) {
          consecutiveWord = { word: '', position: result.word_position - 1 };
        }
        consecutiveWord.word = consecutiveWord.word + result.surface_form;
      } else {
        if (consecutiveWord === null) return;
        pnList.push(consecutiveWord);
        consecutiveWord = null;
      }
    });

    return pnList;
  }
}
