import {
  GetObjectCommand,
  PutObjectCommand,
  S3Client,
} from '@aws-sdk/client-s3';
import { getSignedUrl } from '@aws-sdk/s3-request-presigner';
import { Injectable } from '@nestjs/common';

const _key = (filename: string) => `assets/${filename.replace(/^\//, '')}`;

const EXPIRES_IN_MINUTE = 60 * 60;

@Injectable()
export class StorageService {
  private s3Client: S3Client | null = null;

  constructor() {
    const REGION = 'ap-northeast-1'; //e.g. "us-east-1"
    this.s3Client = new S3Client({ region: REGION });
  }

  async saveImage(filename: string, base64: string) {
    const base64Data = Buffer.from(
      base64.replace(/^data:image\/\w+;base64,/, ''),
      'base64',
    );

    const type = base64.split(';')[0].split('/')[1];

    const key = _key(`${filename}.${type}`);

    await this.s3Client.send(
      new PutObjectCommand({
        Bucket: 'setsuna-diary-assets',
        Key: key,
        ContentEncoding: 'base64',
        ContentType: `image/${type}`,
        Body: base64Data,
      }),
    );

    const url = await getSignedUrl(
      this.s3Client,
      new GetObjectCommand({
        Bucket: 'setsuna-diary-assets',
        Key: key,
      }),
      { expiresIn: 60 },
    );

    return {
      key,
      url: url,
    };
  }

  async getUrlList(keyList: string[]) {
    return Promise.allSettled(
      keyList.map((key) =>
        getSignedUrl(
          this.s3Client,
          new GetObjectCommand({
            Bucket: 'setsuna-diary-assets',
            Key: key,
          }),
          { expiresIn: EXPIRES_IN_MINUTE },
        ),
      ),
    ).then((results) =>
      results
        .filter((result) => result.status === 'fulfilled')
        .map((result) => (result as PromiseFulfilledResult<string>).value),
    );
  }
}
