import { Test, TestingModule } from '@nestjs/testing';
import { AnalysisService } from './analysis';

describe('AnalysisService', () => {
  let service: AnalysisService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [AnalysisService],
    }).compile();

    service = module.get<AnalysisService>(AnalysisService);
  });

  it('分かち書きできるか', async () => {
    const result = await service.extractProperNounList(
      '僕の名前は田中太郎です。皆さんよろしくお願いいたします！好きな食べ物はキムチチャーハンと寿司ですね！ 🤗',
    );
    expect(result.length).not.toEqual(0);
    expect(result.some((r) => r.word === '田中太郎')).toBeTruthy();
  });
});
