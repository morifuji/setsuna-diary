import { Injectable, Scope } from '@nestjs/common';
import * as DataLoader from 'dataloader';

import { StorageService } from './storage';

@Injectable({ scope: Scope.REQUEST })
export class StorageDataLoader {
  dataloader: DataLoader<string, string>;

  constructor(private readonly storageService: StorageService) {
    this.dataloader = new DataLoader<string, string>((keyList: string[]) => {
      return this.storageService.getUrlList(keyList);
    });
  }

  load(key: string) {
    return this.dataloader.load(key);
  }
}
