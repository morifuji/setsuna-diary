export const TEST_AUTHOR_ID = '7496a6fc-66be-4bc3-ac6b-e1d2375013c2';

export const TOKEN_HEADER_NAME = 'x-setsuna-id';

interface A {
  a?: number;
  b: string;
  c: string | null;
}
type NullableToUndefinable<T> = Pick<T, NonNullableKey<T>> & {
  [K in keyof Omit<T, NonNullableKey<T>>]?: NonNullable<T[K]>;
};

export type NonNullableKey<T> = {
  [K in keyof T]-?: null extends T[K] ? never : K;
}[keyof T];
