import { HttpModule } from '@nestjs/axios';
import { Test, TestingModule } from '@nestjs/testing';
import { TopicResolver } from 'src/resolver/resolver.topic';
import { StorageService } from 'src/service/storage';
import { PrismaService } from 'src/prisma/prisma.service';
import { TwitterService } from './twitter.service';

describe('TwitterService', () => {
  let service: TwitterService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [TwitterService, PrismaService, TopicResolver, StorageService],
    }).compile();

    service = module.get<TwitterService>(TwitterService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  it('get data', async () => {
    const [tweets, medias] = await service.fetchlatestTweets();

    expect(tweets.length).toBeGreaterThan(0);

    expect(tweets[0].text).toBeTruthy();
  });

  it('save data', async () => {
    await service.saveTweets();
  });
});
