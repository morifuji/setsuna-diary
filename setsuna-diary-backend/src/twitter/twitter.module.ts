import { Module } from '@nestjs/common';
import { TwitterService } from './twitter.service';
import { HttpModule } from '@nestjs/axios';
import { PrismaService } from 'src/prisma/prisma.service';
import { TopicResolver } from 'src/resolver/resolver.topic';
import { StorageService } from 'src/service/storage';

@Module({
  imports: [HttpModule],
  providers: [TwitterService, PrismaService, TopicResolver, StorageService],
})
export class TwitterModule {}
