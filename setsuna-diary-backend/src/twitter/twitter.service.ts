import { HttpService } from '@nestjs/axios';
import { Injectable } from '@nestjs/common';
import { format, parseISO } from 'date-fns';
import { TopicResolver } from 'src/resolver/resolver.topic';
import { StorageService } from 'src/service/storage';
import { TEST_AUTHOR_ID } from 'src/const';
import { PrismaService } from 'src/prisma/prisma.service';
import { v4 as uuidv4 } from 'uuid';
import { AssetType } from '@prisma/client';
import { Cron } from '@nestjs/schedule';

type Tweet = {
  edit_history_tweet_ids: string[];
  id: string;
  author_id: string;
  text: string;
  created_at: string;
  attachments?: {
    media_keys?: string[];
  };
};
type TwitterResponse = {
  data: Tweet[];
  includes?: {
    media?: {
      media_key: string;
      type: 'photo';
      url: string;
    }[];
  };
};

@Injectable()
export class TwitterService {
  constructor(
    private prismaService: PrismaService,
    private httpService: HttpService,
    private topic: TopicResolver,
    private storageService: StorageService,
  ) {}

  async fetchlatestTweets(usernameList: string[] = ['marooon88']) {
    const data = await Promise.all(
      usernameList.map((username) => {
        return this.httpService.axiosRef
          .get<TwitterResponse>(
            `https://api.twitter.com/2/tweets/search/recent`,
            {
              params: {
                query: `from:${username}`,
                'tweet.fields': 'created_at,entities',
                expansions: 'attachments.media_keys',
                'media.fields': 'url',
              },
              headers: {
                Authorization: `Bearer ${process.env.TWITTER_API_TOKEN}`,
              },
            },
          )
          .then((response) => response.data);
      }),
    );

    return [
      data.flatMap((d) => d.data),
      data.flatMap((d) => d.includes?.media ?? []),
    ] as const;
  }

  @Cron('45 * * * * *')
  async saveTweets(userIdList: string[] = ['marooon88', 'marooon88_tech']) {
    const latestTweet = await this.prismaService.topic.findFirst({
      orderBy: {
        createdAt: 'desc',
      },
      where: {
        source: 'TWITTER',
      },
    });

    const lastFetchedDate = latestTweet?.createdAt;

    const tweetData = await this.fetchlatestTweets(userIdList);
    let tweets = tweetData[0];
    const medias = tweetData[1];

    if (lastFetchedDate !== undefined) {
      console.log(lastFetchedDate);
      console.log(new Date(tweets[0].created_at));
      tweets = tweets.filter((t) => new Date(t.created_at) > lastFetchedDate);
    }

    for (const tweet of tweets) {
      const images = [];
      if (
        tweet.attachments?.media_keys !== undefined &&
        tweet.attachments.media_keys.length > 0
      ) {
        for (const mediaKey of tweet.attachments?.media_keys) {
          const targetMedia = medias.find(
            (media) => media.media_key === mediaKey,
          );
          if (targetMedia) {
            const encoded = await this.topic.fetchBase64ImageFromUrl(
              targetMedia.url,
            );
            images.push(encoded);
          }
        }
      }

      const imageDataList = await Promise.all(
        images.map((base64Image) =>
          this.storageService
            .saveImage(uuidv4(), base64Image)
            .then((imageData) => {
              return {
                key: imageData.key,
                assetType: AssetType.IMAGE,
              };
            }),
        ),
      );

      const pageData = {
        date: format(parseISO(tweet.created_at), 'yyyy-MM-dd'),
        authorId: TEST_AUTHOR_ID,
      };

      console.log(pageData);

      await this.prismaService.topic.create({
        data: {
          content: tweet.text,
          createdAt: parseISO(tweet.created_at),
          source: 'TWITTER',
          page: {
            connectOrCreate: {
              where: {
                date_authorId: pageData,
              },
              create: pageData,
            },
          },
          assets: {
            createMany: {
              data: imageDataList,
            },
          },
        },
      });
    }
  }
}
