import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { GqlExecutionContext } from '@nestjs/graphql';
import { TOKEN_HEADER_NAME } from 'src/const';

@Injectable()
export class AuthGuard implements CanActivate {
  canActivate(context: ExecutionContext) {
    const ctx = GqlExecutionContext.create(context).getContext();
    const userId = ctx.req.headers[TOKEN_HEADER_NAME];

    return !!userId;
  }
}
