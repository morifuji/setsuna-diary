import { PrismaClient } from '@prisma/client';
import * as dateFns from 'date-fns';
import { TEST_AUTHOR_ID } from '../const';
const prisma = new PrismaClient();

async function main() {
  const author = await prisma.author.create({
    data: {
      id: TEST_AUTHOR_ID,
      username: 'morifuji',
      pages: {
        create: [
          {
            date: '2021-03-18',
          },
        ],
      },
    },
  });

  await prisma.topic.create({
    data: {
      content: '##',
      date: '2021-03-18',
      authorId: author.id,
    },
  });

  await prisma.topic.create({
    data: {
      content: 'こんちは！in18日',
      date: '2021-03-18',
      authorId: author.id,
      assets: {
        create: [
          {
            key: 'https://placehold.jp/150x150.png',
            assetType: 'IMAGE',
            metadata: '',
          },
        ],
      },
    },
  });

  await prisma.topic.create({
    data: {
      date: '2021-03-18',
      authorId: author.id,
      content: 'こんちは！in17日',
    },
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
