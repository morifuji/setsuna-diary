import { PrismaClient } from '@prisma/client';
import * as dateFns from 'date-fns';
import { TEST_AUTHOR_ID } from '../const';
const prisma = new PrismaClient();

async function main() {
  [...new Array(10).fill(null)].forEach(async (_, i) => {
    const targetDate = dateFns.subDays(new Date(), i + 20);
    const date = dateFns.format(targetDate, 'yyyy-MM-dd');
    await prisma.page.create({
      data: {
        date: date,
        authorId: TEST_AUTHOR_ID,
        topics: {
          createMany: {
            data: [
              {
                content: `hello_${date}001`,
                createdAt: dateFns.subHours(targetDate, 0),
              },
              {
                content: `hello_${date}002`,
                createdAt: dateFns.subHours(targetDate, 1),
              },
              {
                content: `hello_${date}003`,
                createdAt: dateFns.subHours(targetDate, 2),
              },
              {
                content: `hello_${date}004`,
                createdAt: dateFns.subHours(targetDate, 3),
              },
            ],
          },
        },
      },
    });
  });
}

main()
  .catch((e) => {
    console.error(e);
    process.exit(1);
  })
  .finally(async () => {
    await prisma.$disconnect();
  });
