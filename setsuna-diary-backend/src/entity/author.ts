import { Field, ObjectType } from '@nestjs/graphql';
import { Author as PrismaAuthor } from '@prisma/client';

@ObjectType()
export class Author implements PrismaAuthor {
  @Field()
  id: string;
  @Field()
  username: string;
}
