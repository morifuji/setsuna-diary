import { Field, ObjectType } from '@nestjs/graphql';
import { Topic as PrismaTopic } from '@prisma/client';

@ObjectType()
export class Topic implements Omit<PrismaTopic, 'source'> {
  @Field()
  id: string;

  @Field()
  date: string;
  @Field()
  authorId: string;

  @Field()
  content: string;

  @Field()
  createdAt: Date;

  @Field()
  source: string;
}
