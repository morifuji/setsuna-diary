import { Field, ObjectType } from '@nestjs/graphql';
import { Asset as PrismaAsset, AssetType } from '@prisma/client';

@ObjectType()
export class Asset implements PrismaAsset {
  @Field()
  id: string;

  @Field()
  assetType: AssetType;

  @Field()
  key: string;

  @Field(() => String, { nullable: true })
  metadata: string | null;

  @Field()
  topicId: string;
}
