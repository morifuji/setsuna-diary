import { Field, ObjectType } from '@nestjs/graphql';
import { Page as PrismaPage } from '@prisma/client';

@ObjectType()
export class Page implements PrismaPage {
  @Field()
  authorId: string;
  @Field()
  date: string;
}
