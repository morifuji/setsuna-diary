import { Field, ObjectType } from '@nestjs/graphql';

@ObjectType()
export class Theme {
  @Field()
  position: number;
  @Field()
  word: string;
}
