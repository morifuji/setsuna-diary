type SortOrder = { [k: string]: 'asc' | 'desc' };

export const orderType2OrderByParam = (orderType: string): SortOrder => {
  if (orderType[0] !== '-') {
    return { [orderType]: 'asc' };
  }

  return { [orderType.slice(1)]: 'desc' };
};
