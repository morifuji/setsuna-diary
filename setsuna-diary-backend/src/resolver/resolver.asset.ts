import 'reflect-metadata';
import { Resolver, Mutation, ResolveField, Root, Args } from '@nestjs/graphql';
import {
  HttpException,
  HttpStatus,
  Injectable,
  UseGuards,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthGuard } from 'src/config/guard/guard.auth';
import { User } from 'src/config/decorator/decorator.user';
import { Asset } from 'src/entity/asset';
import { Topic } from 'src/entity/topic';
import { StorageDataLoader } from 'src/service/storage.dataloader';

@Resolver(() => Asset)
@UseGuards(AuthGuard)
@Injectable()
export class AssetResolver {
  constructor(
    private prismaService: PrismaService,
    private readonly storageDataLoader: StorageDataLoader,
  ) {}

  @ResolveField(() => Topic)
  topic(@Root() asset: Asset): Promise<Topic> {
    return this.prismaService.topic.findFirst({
      where: {
        id: asset.topicId,
      },
    });
  }

  @ResolveField(() => String)
  async url(@Root() asset: Asset): Promise<string> {
    return this.storageDataLoader.load(asset.key);
  }

  @Mutation(() => String, { nullable: true })
  async deleteAsset(
    @Args('id') id: string,
    @User() userId: string,
    @Root() asset: Asset,
  ): Promise<string> {
    const authorId = await this.prismaService.topic
      .findFirst({
        where: {
          id: asset.topicId,
        },
        select: {
          authorId: true,
        },
      })
      .then((res) => res.authorId);
    if (authorId !== userId) {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    }

    return this.prismaService.asset
      .delete({
        where: {
          id: id,
        },
      })
      .then(() => id);
  }
}
