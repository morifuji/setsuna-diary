import 'reflect-metadata';
import { v4 as uuidv4 } from 'uuid';
import {
  Resolver,
  Query,
  Mutation,
  Args,
  ResolveField,
  Root,
  InputType,
  Field,
  registerEnumType,
} from '@nestjs/graphql';
import {
  ClassSerializerInterceptor,
  Injectable,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Topic } from 'src/entity/topic';
import { Asset } from 'src/entity/asset';
import { format } from 'date-fns';
import { AuthGuard } from 'src/config/guard/guard.auth';
import { Page } from 'src/entity/page';
import { TEST_AUTHOR_ID } from 'src/const';
import { orderType2OrderByParam } from 'src/utils/orderType2OrderByParam';
import { StorageService } from 'src/service/storage';
import axios from 'axios';
import { Theme } from 'src/entity/theme';

@InputType()
export class TopicCreateInput {
  @Field()
  content: string;

  @Field(() => [AssetInput])
  assets: AssetInput[];
}

enum AssetType {
  IMAGE = 'IMAGE',
  VIDEO = 'VIDEO',
  TWEET = 'TWEET',
}

enum TopicOrderType {
  CREATED_AT_ASC = 'createdAt',
  CREATED_AT_DESC = '-createdAt',
}

@InputType()
class AssetInput {
  @Field(() => AssetType)
  assetType: AssetType;
  @Field()
  content: string;
}
registerEnumType(AssetType, {
  name: 'AssetType',
});

registerEnumType(TopicOrderType, {
  name: 'TopicOrderType',
});

@Resolver(() => Topic)
@UseGuards(AuthGuard)
@Injectable()
export class TopicResolver {
  constructor(
    private prismaService: PrismaService,
    private storageService: StorageService,
  ) {}

  @ResolveField(() => [Asset])
  assets(@Root() topic: Topic): Promise<Asset[]> {
    return this.prismaService.asset.findMany({
      where: {
        topicId: topic.id,
      },
    });
  }

  @ResolveField(() => Page)
  page(@Root() topic: Topic): Promise<Page> {
    return this.prismaService.page.findFirst({
      where: {
        date: topic.date,
        authorId: topic.authorId,
      },
    });
  }

  @ResolveField(() => [Theme])
  async themes(@Root() topic: Topic): Promise<Theme[]> {
    const themeList = await this.prismaService.topicTheme.findMany({
      where: {
        topicId: topic.id,
      },
      include: { theme: true },
    });

    return themeList.map((topicTheme) => {
      return {
        word: topicTheme.theme.word,
        position: topicTheme.position,
      };
    });
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Query(() => [Topic], { nullable: false })
  topics(
    @Args('maxDate', { type: () => Date, nullable: true })
    maxDate?: Date,
    @Args('minDate', { type: () => Date, nullable: true })
    minDate?: Date,
    @Args('order', { type: () => TopicOrderType, nullable: true })
    order?: TopicOrderType,
  ) {
    return this.prismaService.topic.findMany({
      orderBy: orderType2OrderByParam(order || TopicOrderType.CREATED_AT_ASC),
      where: {
        authorId: TEST_AUTHOR_ID,
        createdAt: {
          gte: minDate,
          lte: maxDate,
        },
      },
    });
  }

  @Query(() => [Topic], { nullable: false })
  topicsByDate(@Args('date') date: string) {
    return this.prismaService.topic.findMany({
      where: { date, authorId: TEST_AUTHOR_ID },
    });
  }

  @Mutation(() => Topic)
  async saveTopic(
    @Args('topic') topic: TopicCreateInput,
  ): Promise<Topic | null> {
    const now = new Date();
    const pageData = {
      date: format(now, 'yyyy-MM-dd'),
      authorId: TEST_AUTHOR_ID,
    };

    const imageAssetList = topic.assets.filter(
      (asset) => asset.assetType === AssetType.IMAGE,
    );
    const base64ImageList = await Promise.all(
      imageAssetList.map((asset) =>
        asset.content.startsWith('http')
          ? this.fetchBase64ImageFromUrl(asset.content)
          : asset.content,
      ),
    );
    const imageDataList = await Promise.all(
      base64ImageList.map((base64Image) =>
        this.storageService
          .saveImage(uuidv4(), base64Image)
          .then((imageData) => {
            return {
              key: imageData.key,
              assetType: AssetType.IMAGE,
            };
          }),
      ),
    );
    return this.prismaService.topic.create({
      data: {
        content: topic.content,
        page: {
          connectOrCreate: {
            where: {
              date_authorId: pageData,
            },
            create: pageData,
          },
        },
        assets: {
          createMany: {
            data: imageDataList,
          },
        },
      },
    });
  }

  @Mutation(() => Page, { nullable: true })
  async deleteTopic(@Args('id') id: string): Promise<Page> {
    return this.prismaService.topic.delete({
      where: {
        id: id,
      },
      include: {
        assets: true,
      },
    });
  }

  fetchBase64ImageFromUrl(imageUrl: string) {
    return axios
      .get(imageUrl, {
        responseType: 'arraybuffer',
      })
      .then(
        (response) =>
          `data:${response.headers[
            'content-type'
          ].toLowerCase()};base64,${Buffer.from(
            response.data,
            'binary',
          ).toString('base64')}`,
      );
  }
}
