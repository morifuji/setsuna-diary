import 'reflect-metadata';
import { Resolver, Mutation, ResolveField, Root } from '@nestjs/graphql';
import { Injectable, UseGuards } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { AuthGuard } from 'src/config/guard/guard.auth';
import { Page } from 'src/entity/page';
import { User } from 'src/config/decorator/decorator.user';
import { Author } from 'src/entity/author';

@Resolver(() => Author)
@UseGuards(AuthGuard)
@Injectable()
export class AuthorResolver {
  constructor(private prismaService: PrismaService) {}

  @ResolveField(() => [Page])
  pagesByDate(@Root() author: Author): Promise<Page[]> {
    return this.prismaService.page.findMany({
      where: {
        authorId: author.id,
      },
    });
  }

  @Mutation(() => String, { nullable: true })
  async deleteAuthor(@User() userId: string): Promise<string> {
    return this.prismaService.author
      .delete({
        where: {
          id: userId,
        },
      })
      .then(() => userId);
  }
}
