import 'reflect-metadata';
import { Resolver, Mutation, Args, ResolveField, Root } from '@nestjs/graphql';
import { Injectable, Query, UseGuards } from '@nestjs/common';
import { PrismaService } from 'src/prisma/prisma.service';
import { Topic } from 'src/entity/topic';
import { Asset } from 'src/entity/asset';
import { AuthGuard } from 'src/config/guard/guard.auth';
import { Page } from 'src/entity/page';
import { User } from 'src/config/decorator/decorator.user';
import { Author } from 'src/entity/author';

@Resolver(() => Page)
@UseGuards(AuthGuard)
@Injectable()
export class PageResolver {
  constructor(private prismaService: PrismaService) {}

  @ResolveField(() => [Asset])
  topics(@Root() page: Page): Promise<Topic[]> {
    return this.prismaService.topic.findMany({
      where: {
        date: page.date,
        authorId: page.authorId,
      },
    });
  }

  @ResolveField(() => Author)
  author(@Root() page: Page): Promise<Author> {
    return this.prismaService.author.findFirst({
      where: {
        id: page.authorId,
      },
    });
  }

  @Mutation(() => Author, { nullable: true })
  async deletePage(
    @Args('date') date: string,
    @User() userId: string,
  ): Promise<Author> {
    return this.prismaService.page
      .delete({
        where: {
          date_authorId: {
            date,
            authorId: userId,
          },
        },
      })
      .author();
  }
}
