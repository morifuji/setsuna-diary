import 'reflect-metadata';
import { Resolver, Query, Args, ResolveField, Root } from '@nestjs/graphql';
import {
  ClassSerializerInterceptor,
  Injectable,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { AuthGuard } from 'src/config/guard/guard.auth';
import { Theme } from 'src/entity/theme';
// import { AnalysisService } from 'src/service/analysis';
import { Topic } from 'src/entity/topic';
import { PrismaService } from 'src/prisma/prisma.service';

@Resolver(() => Theme)
@UseGuards(AuthGuard)
@Injectable()
export class ThemeResolver {
  constructor(
    // private analysisService: AnalysisService,
    private prismaService: PrismaService,
  ) {}

  @UseInterceptors(ClassSerializerInterceptor)
  @Query(() => [Theme], { nullable: false })
  getThemeList(
    @Args('text', { nullable: false })
    text: string,
  ) {
    // return this.analysisService.extractProperNounList(text);
  }

  @UseInterceptors(ClassSerializerInterceptor)
  @Query(() => Theme, { nullable: true })
  findByWord(
    @Args('word')
    word: string,
  ) {
    return this.prismaService.theme.findFirst({
      where: { word },
    });
  }

  @ResolveField(() => [Topic])
  async topics(@Root() theme: Theme): Promise<Topic[]> {
    const topicList = await this.prismaService.topicTheme.findMany({
      where: {
        theme: { word: theme.word },
      },
      include: { topic: true },
    });

    return topicList.map((topicTheme) => {
      return topicTheme.topic;
    });
  }
}
