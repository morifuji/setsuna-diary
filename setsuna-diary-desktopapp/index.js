const { app, BrowserWindow, globalShortcut, ipcMain } = require('electron')
const path = require("path")
const fetch = require('electron-fetch');

const download = async (url) => {
    const response = await fetch(url);
    const responseType = response.headers.get("content-type") || response.headers.get("Content-Type")
    return `data:${responseType};base64,${(await response.buffer()).toString('base64')}`
}


/**
 * @type {BrowserWindow|null}
 */
let inputWindow = null

const createInputWindow = () => {
    // すでに表示されている
    if (inputWindow !== null) {
        inputWindow.focus()
    }
    inputWindow = new BrowserWindow({
        center: true,
        transparent: true,
        minHeight: 204,
        maxHeight: 204,
        minWidth: 700,
        maxWidth: 700,
        frame: false,
        icon: path.resolve(__dirname, '..', 'icon.png'),
        webPreferences: {
            preload: path.resolve(__dirname, 'preload.js')
        }
    })
    inputWindow.loadFile('view/index.html')

    // for debug
    // inputWindow.webContents.openDevTools();

    inputWindow.on('close', (e) => {
        inputWindow = null
    });
}
app.whenReady().then(() => {
    globalShortcut.register('CommandOrControl+I', () => {
        createInputWindow()
    })

    createInputWindow()

    app.on('activate', () => {
        if (BrowserWindow.getAllWindows().length === 0) createInputWindow()
    })


    ipcMain.on('closeWindow', () => {
        if (inputWindow === null) return

        inputWindow.close()
    })
    ipcMain.on('onChangeWindowHeight', () => {
        if (inputWindow === null) return

        inputWindow.maximize()
        inputWindow.center()
    })

    ipcMain.on('notifyMessage', (message) => {
        console.log("notifyMessage", message)
    })

    ipcMain.handle('convertUrlToBase64', async (event, url) => {
        console.log(url)
        const base64 = await download(url)
        return base64
    })
})


app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') app.quit()
})


