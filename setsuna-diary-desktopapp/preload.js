const { contextBridge, ipcRenderer } = require('electron')

console.log("laod!!! 🚀")

new Notification("Hello! What's up!?")

contextBridge.exposeInMainWorld('electronAPI', {
    closeWindow: () => {
        console.log("hogehoge!!!! 🙏")
        ipcRenderer.send("closeWindow")
    },
    onChangeWindowHeight: () => {
        console.log("onChangeWindowHeight!!!! 🙏")
        ipcRenderer.send("onChangeWindowHeight")
    },
    convertUrlToBase64: (url) => {
        console.log("convertUrlToBase64!!!! 🙏")
        return ipcRenderer.invoke("convertUrlToBase64", url)
    }
})

console.log("done!!! 🚀")