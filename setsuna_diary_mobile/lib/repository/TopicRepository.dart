import 'dart:convert';
import 'dart:developer';

import '../models/Topic.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_dotenv/flutter_dotenv.dart';

abstract class TopicRepository {
  Future<List<Topic>> fetchTopics();

  Future<void> addTopic(String content, List<String> imageListBase64Encoded);
}

var GRAPHQL_ENDPOINT = dotenv.env['GRAPHQL_ENDPOINT']!;
var SETSUNA_USER_ID = dotenv.env['SETSUNA_USER_ID']!;

class HttpTopicRepository implements TopicRepository {
  @override
  Future<List<Topic>> fetchTopics() async {
    final response = await http.post(Uri.parse(GRAPHQL_ENDPOINT),
        headers: {
          "x-setsuna-id": SETSUNA_USER_ID,
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
        body: jsonEncode(<String, dynamic>{
          "query": """query(\$minDate: DateTime) {
  topics(order: CREATED_AT_DESC, minDate: \$minDate){
    authorId,
    id,
    page{
      date
    },
    content,
    createdAt,
    assets{
      id,
      assetType,
      key,
      url
    }
  }
}
""",
          "variables": {"minDate": "2020-01-01"}
        }));

    List<dynamic> rawTopicList = jsonDecode(response.body)['data']['topics'];

    var topicList = rawTopicList
        .map((rawTopicData) => Topic.fromJson(rawTopicData))
        .toList();

    return topicList;
  }

  @override
  Future<void> addTopic(
      String content, List<String> imageListBase64Encoded) async {
    // FIXME
    var assetString = imageListBase64Encoded
        .map((base64) => '{assetType: IMAGE,content: "$base64"}')
        .join(",");
    final response = await http.post(Uri.parse(GRAPHQL_ENDPOINT),
        headers: {
          "x-setsuna-id": SETSUNA_USER_ID,
          "Content-Type": "application/json",
          "Accept": "application/json",
        },
        body: jsonEncode(<String, dynamic>{
          "query": """
mutation hoge(\$content: String!){
  saveTopic(topic: {content: \$content, assets: [
    $assetString
  ]}){
    id
    content
    assets{
      url
    }
  }
}
""",
          "variables": {"content": content}
        }));
  }
}
