class Asset {
  final String id;
  final String assetType;
  final String url;

  const Asset({required this.id, required this.assetType, required this.url});

  factory Asset.fromJson(Map<String, dynamic> json) {
    return Asset(
      id: json['id'],
      assetType: json['assetType'],
      url: json['url'],
    );
  }
}
