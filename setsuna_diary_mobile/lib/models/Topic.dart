import 'dart:developer';

import 'package:setsuna_diary_mobile/models/Asset.dart';
import 'package:faker/faker.dart';

class Topic {
  final String id;
  final String date;
  final String authorId;
  final String content;
  final String createdAt;
  final List<Asset> assets;

  const Topic({
    required this.id,
    required this.date,
    required this.authorId,
    required this.content,
    required this.createdAt,
    required this.assets,
  });

  factory Topic.fromJson(Map<String, dynamic> json) {
    List<Asset> assets = (json['assets'] as List<dynamic>)
        .map((asset) => Asset.fromJson(asset))
        .toList();
    // NOTE: for demo
    var content =
        (json['createdAt'] as String).compareTo("2022-06-13T11:00") == -1
            ? faker.lorem.sentence()
            : json['content'];
    return Topic(
        id: json['id'],
        date: json['page']['date'],
        authorId: json['authorId'],
        content: content,
        createdAt: json['createdAt'],
        assets: assets);
  }
}

class Album {
  final int userId;
  final int id;
  final String title;

  const Album({
    required this.userId,
    required this.id,
    required this.title,
  });

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
      userId: json['userId'],
      id: json['id'],
      title: json['title'],
    );
  }
}
