import 'package:intl/date_symbol_data_local.dart';

import 'package:flutter/material.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

import 'const.dart';
import 'layout.dart';

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  RootState createState() => RootState();
}

class RootState extends State<MyApp> {
  @override
  void initState() {
    initializeDateFormatting("ja_JP");
    super.initState();
  }

  Future<void> initialize() async {
    return dotenv.load(fileName: ".env");
  }

  @override
  Widget build(BuildContext context) {
    return MediaQuery(
        data: const MediaQueryData(),
        child: MaterialApp(
            darkTheme: ThemeData(
                outlinedButtonTheme: OutlinedButtonThemeData(
                  style: OutlinedButton.styleFrom(
                      primary: PRIMARY_COLOR,
                      textStyle: const TextStyle(fontWeight: FontWeight.bold)),
                ),
                brightness: Brightness.dark,
                primaryColor: PRIMARY_COLOR),
            themeMode: ThemeMode.dark,
            home: FutureBuilder(
              future: initialize(),
              builder: (BuildContext context, AsyncSnapshot<void> snapshot) {
                if (snapshot.connectionState != ConnectionState.done) {
                  return const Center(child: Text("Loading..."));
                }

                return const Layout();
              },
            )));
  }
}
