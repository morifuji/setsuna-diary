import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:setsuna_diary_mobile/models/Topic.dart';
import 'package:setsuna_diary_mobile/widgets/topic_card.dart';

class TopicCardListWidget extends StatelessWidget {
  TopicCardListWidget(
      {Key? key, required this.topicList, required this.onTapImage})
      : super(key: key);

  List<Topic> topicList;
  Function(String) onTapImage;

  Widget buildTopicCard(BuildContext context, Topic topic) {
    return TopicCardWidget(
      topic: topic,
      onTapImage: onTapImage,
    );
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
        children: topicList
            .map<Widget>((topic) => buildTopicCard(context, topic))
            .toList());
  }
}
