import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:setsuna_diary_mobile/models/Topic.dart';
import 'package:table_calendar/table_calendar.dart';

class CalendarWidget extends StatelessWidget {
  CalendarWidget({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 400,
      child: Center(
          child: TableCalendar(
        locale: "ja_JP",
        firstDay: DateTime.utc(2010, 10, 16),
        lastDay: DateTime.utc(2030, 3, 14),
        focusedDay: DateTime.now(),
      )),
    );
  }
}
