import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:setsuna_diary_mobile/models/Topic.dart';
import 'dart:io' as Io;

import '../const.dart';

class FormWidget extends StatelessWidget {
  FormWidget({
    Key? key,
    required this.content,
    required this.mediaFileListShared,
    required this.mediaFileListInputed,
    required this.onAddMediaFile,
    required this.onAddTopic,
    required this.onInputContent,
  }) : super(key: key);

  String content;

  Function(List<XFile>) onAddMediaFile;
  Function() onAddTopic;
  Function(String) onInputContent;

  List<SharedMediaFile> mediaFileListShared;
  List<XFile> mediaFileListInputed;

  final ImagePicker imagePicker = ImagePicker();

  @override
  Widget build(BuildContext context) {
    var contentController = TextEditingController(text: content);
    contentController.addListener(() {
      onInputContent(contentController.value.text);
    });
    return Column(mainAxisSize: MainAxisSize.min, children: [
      Container(
        padding: const EdgeInsets.all(8),
        child: Row(
          children: [
            const Padding(
              padding: EdgeInsets.only(top: 32, left: 8, right: 8, bottom: 8),
              child: Text(
                "🖋",
                style: TextStyle(fontSize: 28),
              ),
            ),
            Expanded(
              child: TextFormField(
                cursorColor: PRIMARY_COLOR,
                autofocus: true,
                controller: contentController,
                maxLines: null,
                keyboardType: TextInputType.multiline,
                decoration: const InputDecoration(
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: PRIMARY_COLOR)),
                  border: UnderlineInputBorder(
                      borderSide: BorderSide(color: PRIMARY_COLOR)),
                  labelStyle: TextStyle(color: Colors.white70),
                  labelText: '今何してる？👀',
                ),
              ),
            )
          ],
        ),
      ),
      Wrap(children: [
        ...mediaFileListShared
            .map<Widget>((e) => Image.file(Io.File(e.path),
                width: 100, height: 100, fit: BoxFit.contain))
            .toList(),
        ...mediaFileListInputed
            .map<Widget>((e) => Image.file(Io.File(e.path),
                width: 100, height: 100, fit: BoxFit.contain))
            .toList()
      ]),
      Row(children: [
        Padding(
            padding: const EdgeInsets.only(left: 10),
            child: OutlinedButton.icon(
              icon: const Icon(Icons.photo_album),
              label: const Text("Pick images"),
              onPressed: () async {
                var imageList = await imagePicker.pickMultiImage(
                    maxWidth: 1000, maxHeight: 1000, imageQuality: 80);
                if (imageList == null) return;
                onAddMediaFile(imageList);
              },
            )),
        const Spacer(),
        Padding(
            padding: const EdgeInsets.only(right: 10),
            child: ElevatedButton.icon(
              style: ButtonStyle(
                  backgroundColor:
                      MaterialStateProperty.all<Color>(PRIMARY_COLOR)),
              icon: const Icon(Icons.save),
              onPressed: () {
                onAddTopic();
              },
              label: const Text('Save'),
            )),
      ]),
      Padding(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      )
    ]);
  }
}
