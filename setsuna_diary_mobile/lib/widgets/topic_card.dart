import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:setsuna_diary_mobile/models/Topic.dart';

class TopicCardWidget extends StatelessWidget {
  TopicCardWidget({Key? key, required this.topic, required this.onTapImage})
      : super(key: key);

  final DateFormat dateFormat = DateFormat('MM月dd日(E)', "ja_JP");
  final DateFormat timeFormat = DateFormat('HH:mm', "ja_JP");

  final Topic topic;
  final Function(String) onTapImage;

  @override
  Widget build(BuildContext context) {
    var localDatetime = DateTime.parse(topic.createdAt).toLocal();
    return Column(
      children: [
        Container(
            padding: const EdgeInsets.all(8),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Text(dateFormat.format(localDatetime),
                        textAlign: TextAlign.right,
                        style: const TextStyle(
                            color: Colors.white38,
                            fontWeight: FontWeight.bold)),
                    Text(timeFormat.format(localDatetime),
                        textAlign: TextAlign.right,
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                  ],
                ),
                Expanded(
                    child: Padding(
                        padding: const EdgeInsets.only(left: 8),
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(topic.content),
                              Wrap(
                                  children: topic.assets
                                      .where(
                                          (asset) => asset.assetType == "IMAGE")
                                      .map((asset) => GestureDetector(
                                          onTap: () {
                                            onTapImage(asset.url);
                                          },
                                          child: Image(
                                            height: 50,
                                            width: 50,
                                            image: NetworkImage(asset.url),
                                          )))
                                      .toList()),
                            ])))
              ],
            ))
      ],
    );
  }
}
