import 'dart:ui';

const PRIMARY_COLOR = Color.fromRGBO(201, 66, 255, 1);
const PRIMARY_COLOR_RED = Color.fromARGB(255, 255, 66, 208);
const PRIMARY_COLOR_BLUE = Color.fromARGB(255, 138, 66, 255);
