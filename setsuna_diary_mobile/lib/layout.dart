import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:image_picker/image_picker.dart';
import 'package:setsuna_diary_mobile/widgets/calendar.dart';
import 'package:setsuna_diary_mobile/widgets/form.dart';
import 'package:setsuna_diary_mobile/widgets/topic_card_list.dart';
import 'dart:convert';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as tz_latest;

import 'package:flutter/material.dart';
import 'dart:async';
import 'package:photo_view/photo_view.dart';

import 'package:receive_sharing_intent/receive_sharing_intent.dart';
import 'package:setsuna_diary_mobile/repository/TopicRepository.dart';
import 'dart:io' as io;

import 'const.dart';
import 'models/Topic.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();
const IOSInitializationSettings initializationSettingsIOS =
    IOSInitializationSettings();

class Layout extends StatefulWidget {
  const Layout({Key? key}) : super(key: key);

  @override
  LayoutState createState() => LayoutState();
}

class LayoutState extends State<Layout> {
  late StreamSubscription _intentDataStreamSubscription;
  late Future<List<Topic>> futureTopicList;

  TopicRepository topicRepository = HttpTopicRepository();

  List<SharedMediaFile> mediaFileListShared = [];
  List<XFile> mediaFileListInputed = [];

  String topicContent = "";

  String? displayingImageUrl;

  onAddMediaFileInput(List<XFile> imageList, StateSetter setModalState) {
    setModalState(() {
      mediaFileListInputed.addAll(imageList);
    });
  }

  onInputContent(String content) {
    setState(() {
      topicContent = content;
    });
  }

  @override
  void initState() {
    initLocalNotification();
    super.initState();

    futureTopicList = topicRepository.fetchTopics();

    // For sharing images coming from outside the app while the app is in the memory
    _intentDataStreamSubscription = ReceiveSharingIntent.getMediaStream()
        .listen((List<SharedMediaFile> value) async {
      setState(() {
        mediaFileListShared =
            value.where((file) => file.type == SharedMediaType.IMAGE).toList();
      });

      showFormModal();
    }, onError: (err) {
      print("getIntentDataStream error: $err");
    });

    // For sharing images coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialMedia()
        .then((List<SharedMediaFile> value) async {
      setState(() {
        mediaFileListShared =
            value.where((file) => file.type == SharedMediaType.IMAGE).toList();
      });

      showFormModal();
    });

    // For sharing or opening urls/text coming from outside the app while the app is in the memory
    _intentDataStreamSubscription =
        ReceiveSharingIntent.getTextStream().listen((String value) {
      showFormModal();
    }, onError: (err) {
      print("getLinkStream error: $err");
    });

    // For sharing or opening urls/text coming from outside the app while the app is closed
    ReceiveSharingIntent.getInitialText().then((String? value) {
      if (value == null) return;
      showFormModal();
    });
  }

  @override
  void dispose() {
    _intentDataStreamSubscription.cancel();
    super.dispose();
  }

  Widget buildTopicList(BuildContext context) {
    var topicListBuilder = FutureBuilder<List<Topic>>(
        future: futureTopicList,
        builder: (_context, snapshot) {
          if (snapshot.hasData) {
            return TopicCardListWidget(
                topicList: snapshot.data!, onTapImage: onTapImage);
          }
          if (snapshot.hasError) {
            return Text('${snapshot.error}');
          }

          return const Center(child: CircularProgressIndicator());
        });

    return topicListBuilder;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          flexibleSpace: Container(
            decoration: const BoxDecoration(
              gradient: LinearGradient(
                begin: Alignment.bottomLeft,
                end: Alignment.topRight,
                colors: [PRIMARY_COLOR_RED, PRIMARY_COLOR_BLUE],
              ),
            ),
          ),
          title:
              const Text('刹那日記', style: TextStyle(fontWeight: FontWeight.bold)),
        ),
        body: displayingImageUrl == null
            ? buildTopicList(context)
            : PhotoView(
                imageProvider: NetworkImage(displayingImageUrl!),
              ),
        floatingActionButton: Builder(builder: buildFab));
  }

  Widget buildFab(BuildContext context) {
    return displayingImageUrl == null
        ? Column(mainAxisAlignment: MainAxisAlignment.end, children: [
            FloatingActionButton(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Colors.white,
              onPressed: () {
                showModalBottomSheet(
                    context: context,
                    isScrollControlled: true,
                    builder: (BuildContext context) {
                      return CalendarWidget();
                    });
              },
              child: const Icon(Icons.calendar_month),
            ),
            const SizedBox(
              height: 20,
            ),
            FloatingActionButton(
              backgroundColor: Theme.of(context).primaryColor,
              foregroundColor: Colors.white,
              onPressed: () {
                showFormModal();
              },
              child: const Icon(Icons.add),
            ),
          ])
        : FloatingActionButton(
            backgroundColor: Theme.of(context).primaryColor,
            foregroundColor: Colors.white,
            onPressed: () {
              setState(() {
                displayingImageUrl = null;
              });
            },
            child: const Icon(Icons.close_fullscreen),
          );
  }

  void addTopic(
      String content, BuildContext context, StateSetter setModalState) async {
    var imageListBase64String = [
      ...mediaFileListInputed
          .map((file) => convertImageToBase64String(file.path)),
      ...mediaFileListShared
          .map((file) => convertImageToBase64String(file.path))
    ].toList();

    await topicRepository.addTopic(content, imageListBase64String);

    Navigator.pop(context);

    final scaffold = ScaffoldMessenger.of(context);
    scaffold.showSnackBar(
      SnackBar(
        duration: const Duration(seconds: 3),
        content: const Text('登録しました！'),
        action: SnackBarAction(
            label: 'CLOSE', onPressed: scaffold.hideCurrentSnackBar),
      ),
    );

    setState(() {
      mediaFileListInputed = [];
      mediaFileListShared = [];
      topicContent = "";
      futureTopicList = topicRepository.fetchTopics();
    });
    setModalState(() {
      mediaFileListInputed = [];
      mediaFileListShared = [];
      topicContent = "";
    });
  }

  onTapImage(url) {
    setState(() {
      displayingImageUrl = url;
    });
  }

  selectNotification(String? payload) {
    return showFormModal();
  }

  void initLocalNotification() async {
    const InitializationSettings initializationSettings =
        InitializationSettings(iOS: initializationSettingsIOS);
    await flutterLocalNotificationsPlugin.initialize(initializationSettings,
        onSelectNotification: selectNotification);

    await flutterLocalNotificationsPlugin
        .resolvePlatformSpecificImplementation<
            IOSFlutterLocalNotificationsPlugin>()
        ?.requestPermissions(
          alert: true,
          badge: true,
          sound: true,
        );
    tz_latest.initializeTimeZones();

    flutterLocalNotificationsPlugin.cancelAll();

    flutterLocalNotificationsPlugin.zonedSchedule(
        0,
        "刹那日記",
        "今日を一言で言うと？",
        tz.TZDateTime.from(DateTime(2025, 4, 21, 23, 04), tz.local),
        const NotificationDetails(),
        uiLocalNotificationDateInterpretation:
            UILocalNotificationDateInterpretation.absoluteTime,
        androidAllowWhileIdle: true,
        matchDateTimeComponents: DateTimeComponents.time);
  }

  String convertImageToBase64String(String filePath) {
    String ext = filePath.split('.').last;

    return "data:image/$ext;base64," +
        base64.encode(io.File(filePath).readAsBytesSync());
  }

  Future showFormModal() {
    return showModalBottomSheet(
        context: context,
        isScrollControlled: true,
        builder: (BuildContext context) {
          return StatefulBuilder(
              builder: (BuildContext context, StateSetter setModalState) {
            return FormWidget(
              content: topicContent,
              mediaFileListShared: mediaFileListShared,
              mediaFileListInputed: mediaFileListInputed,
              onAddMediaFile: (List<XFile> imageList) {
                onAddMediaFileInput(imageList, setModalState);
              },
              onAddTopic: () {
                addTopic(topicContent, context, setModalState);
              },
              onInputContent: onInputContent,
            );
          });
        });
  }
}
